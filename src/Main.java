import me.mawood.game.*;

import java.awt.*;
import java.util.*;

/**
 * AI-CW-1 - PACKAGE_NAME
 * Created by matthew on 06/10/16.
 */
final class Main
{
    private static final boolean SIMULTANEOUS = false;

    public static void main(String[] args) throws Exception
    {
        /*ArrayList<Tile> tiles = new ArrayList<>(); // prepare a tile store
        tiles.add(new Tile("A",new Point(1,1),new Point(2,1)));

        State startState =
                new GameState(tiles, // setup the game state
                    new Point(4,1), // set actor start position
                        4,1); // set board size*/

        ArrayList<Tile> tiles = new ArrayList<>(); // prepare a tile store
        tiles.add(new Tile("A",new Point(1,1),new Point(2,3)));
        tiles.add(new Tile("B",new Point(2,1),new Point(2,2)));
        tiles.add(new Tile("C",new Point(3,1),new Point(2,1)));

        tiles.add(new Tile("X",new Point(4,1)));
        tiles.add(new Tile("X",new Point(4,2)));
        tiles.add(new Tile("X",new Point(4,3)));

        tiles.add(new Tile("X",new Point(6,4)));
        tiles.add(new Tile("X",new Point(6,3)));
        tiles.add(new Tile("X",new Point(6,2)));

        tiles.add(new Tile("X",new Point(8,1)));
        tiles.add(new Tile("X",new Point(8,2)));
        tiles.add(new Tile("X",new Point(8,3)));

        State startState =
                new GameState(tiles, // setup the game state
                        new Point(9,1), // set actor start position
                        9,4); // set board size*/

        //State startState = formSpecStartState(2,8);

        Node startNode = new GameNode(startState); // create a node from the state

        //testAll(startNode);
        SearchExecutor thread;
        thread = new SearchExecutor(SearchExecutor.SearchMethod.IDS, startNode, SearchExecutor.ImageType.COLLAGE);
        thread.start();
        /*if (!SIMULTANEOUS) while(!thread.isFinished()) Thread.sleep(100);
        thread = new SearchExecutor(SearchExecutor.SearchMethod.DFS, startNode, SearchExecutor.ImageType.COLLAGE);
        thread.start();
        if (!SIMULTANEOUS) while(!thread.isFinished()) Thread.sleep(100);
        thread = new SearchExecutor(SearchExecutor.SearchMethod.BFS, startNode, SearchExecutor.ImageType.COLLAGE);
        thread.run();
        if (!SIMULTANEOUS) while(!thread.isFinished()) Thread.sleep(100);
        thread = new SearchExecutor(SearchExecutor.SearchMethod.ASTAR, startNode, SearchExecutor.ImageType.COLLAGE);
        thread.start();*/


    }

    static GameState formSpecStartState(int size, int extraWidth) // size = 3 for the specs start state
    {
        if (size < 2) size = 2; // ensure the size is allowable
        if (size > 26) size = 26;

        ArrayList<Tile> tiles = new ArrayList<>(); // prepare a tile store

        for(int i = 0; i<size;i++)
        {
            tiles.add(new Tile(String.valueOf((char)(i + 'A')),new Point(1+i,1),new Point(2,size-i)));
        }
        return new GameState(tiles,new Point(size+1+extraWidth,1),size+1+extraWidth, size+1);

    }
}
