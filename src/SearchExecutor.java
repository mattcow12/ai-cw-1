import me.mawood.game.GameNode;
import me.mawood.game.Node;

import java.io.File;
import java.io.IOException;

/**
 * ai-cw-1 - PACKAGE_NAME
 * Created by MAWood on 30/11/2016.
 */
public class SearchExecutor extends Thread
{
    enum SearchMethod
    {
        BFS,DFS,IDS,ASTAR
    }
    enum ImageType
    {
        NONE,GIF,COLLAGE
    }
    private final SearchMethod method;
    private final Node root;
    private boolean finished;
    private final ImageType imageType;

    SearchExecutor(SearchMethod method, Node root, ImageType imageType)
    {
        this.method = method;
        this.root = new GameNode(root.getState());
        this.finished = false;
        this.imageType = imageType;
    }

    @Override
    public void run()
    {
        super.run();

        System.out.println("Starting " + method.name());

        long time = System.nanoTime(); // store the time before the search begins

        Node endNode = search(root); // run the search

        time = System.nanoTime() - time; // calculate the time passed since the start of the search

        if(endNode != null) // if a solution was found
        {
            System.out.println( method.name() + " : " +
                    "After expanding " + endNode.getExpansions() +
                            " nodes a solution was found at depth: " + endNode.getDepth() +
                            "\nit took " + time / 1000000f + "ms"); // output data

            switch (imageType)
            {
                case NONE:
                    return;
                case GIF:
                    try
                    {
                        StateRenderer.makeGifOfSolution(
                                (GameNode) endNode,
                                new File( method.name() + ".gif")); // create a GIF of the  solution
                    } catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                    break;
                case COLLAGE:
                    try
                    {
                        StateRenderer.makeCollageOfSolution(
                                (GameNode) endNode,
                                new File( method.name() + ".png")); // create a GIF of the  solution
                    } catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                    break;
            }

            System.out.println(method.name() + " : " + "Solution saved as " + imageType.name()); // alert the user the gif is created
        } else {
            System.out.println(method.name() + " : " + "No solution was found");
        }
        this.interrupt();
        this.finished = true;

    }

    private Node search(Node root)
    {
        switch (method)
        {
            case ASTAR:
                return TreeSearchAlgorithms.aStar(root);
            case IDS:
                return TreeSearchAlgorithms.IDS(root);
            case DFS:
                return TreeSearchAlgorithms.DFS(root);
            case BFS:
                return TreeSearchAlgorithms.BFS(root);
        }
        return null;
    }

    boolean isFinished()
    {
        return finished;
    }
}
