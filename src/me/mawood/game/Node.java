package me.mawood.game;

/**
 * ai-cw-1 - PACKAGE_NAME
 * Created by MAWood on 23/10/2016.
 */
public abstract class Node
{
    public abstract Node[] expand();
    public abstract Node parent();
    public abstract boolean isSolution();
    public abstract int getHeuristic();
    public abstract State getState();
    public abstract long getExpansions();
    public abstract int getDepth();
}
