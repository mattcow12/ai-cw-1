package me.mawood.game;

/**
 * ai-cw-1 - PACKAGE_NAME
 * Created by MAWood on 23/10/2016.
 */
public abstract class State
{
    public abstract State[] getPossibleNextStates();

    public abstract boolean isEndState();

    public abstract int distanceFromGoal();
}
