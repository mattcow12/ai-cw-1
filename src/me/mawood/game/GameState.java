package me.mawood.game;

import java.awt.*;
import java.util.ArrayList;

/**
 * ai-cw-1 - PACKAGE_NAME
 * Created by MAWood on 23/10/2016.
 */


public class GameState extends State
{
    private final Point playerPos;
    private final ArrayList<Tile> tiles;
    private final int width;
    private final int height;

    public GameState(ArrayList<Tile> tiles, Point playerPos, int width, int height)
    {
        this.tiles = tiles;
        this.playerPos = playerPos;
        this.width = width;
        this.height = height;
    }

    @Override
    public State[] getPossibleNextStates()
    {
        ArrayList<State> states = new ArrayList<>();

        // consider left move
        if(playerPos.x > 1  && isMovablePos(new Point(playerPos.x - 1, playerPos.y )))
        {
            states.add(considerPlayerMove(new Point(playerPos.x - 1, playerPos.y)));
        }
        // consider right move
        if(playerPos.x < width  && isMovablePos(new Point(playerPos.x + 1, playerPos.y)))
        {
            states.add(considerPlayerMove(new Point(playerPos.x + 1, playerPos.y)));
        }
        // consider up move
        if(playerPos.y < height  && isMovablePos(new Point(playerPos.x, playerPos.y + 1)))
        {
            states.add(considerPlayerMove(new Point(playerPos.x, playerPos.y + 1)));
        }
        // consider down move
        if(playerPos.y > 1  && isMovablePos(new Point(playerPos.x, playerPos.y - 1)))
        {
            states.add(considerPlayerMove(new Point(playerPos.x, playerPos.y - 1)));
        }
        State[] output = new State[states.size()];
        for(int i = 0; i<states.size(); i++) output[i] = states.get(i);
        return output;
    }

    @Override
    public boolean isEndState()
    {
        return distanceFromGoal() == 0;
    }

    @Override
    public int distanceFromGoal()
    {
        int distance = 0;
        for (Tile tile:tiles) distance += tile.getManhattanDistanceFromGoal(); // sums the distances
        return distance;
    }

    private GameState considerPlayerMove(Point newPos)
    {
        if (newPos.distance(playerPos) > 1) throw new IllegalStateException(); // if the move is illegal

        ArrayList<Tile> newTiles = (ArrayList<Tile>)tiles.clone();

        Tile moving = null;
        for (Tile tile:newTiles)
        {
            if(tile.getPosition().equals(newPos))
            {
                moving = tile; // if there is a tile in the new position store it
                break;
            }
        }
        if (moving != null)
        {
            newTiles.remove(moving);
            newTiles.add(new Tile(moving.getName(),playerPos,moving.getGoalPos())); // move it to where the character was

        }

        return new GameState(newTiles, newPos, width, height); // if the tile is empty make no changes to tiles

    }

    public String toString()
    {
        String output = "Player Pos " + playerPos.toString();
        for (Tile tile:tiles)
        {
            output += "\n at " + tile.getPosition().toString() + " is " + tile.getName();
        }
        return output;
    }

    private boolean isMovablePos(Point point)
    {
        for(Tile tile:tiles) // checks if the point contains a wall
        {
            if(tile.getPosition().equals(point)) return tile.isMovable();
        }
        return true;
    }

    public Point getPlayerPos()
    {
        return playerPos;
    }

    public ArrayList<Tile> getTiles()
    {
        return tiles;
    }

    public int getWidth()
    {
        return width;
    }

    public int getHeight()
    {
        return height;
    }
}
