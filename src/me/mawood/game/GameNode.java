package me.mawood.game;

/**
 * ai-cw-1 - PACKAGE_NAME
 * Created by MAWood on 23/10/2016.
 */
public class GameNode extends Node
{
    private final State state;
    private final int depth;
    private final Node parent;
    private static long expansions = 0;

    public GameNode(State state)
    {
        this(state,0, null);
        expansions = 0;
    }

    private GameNode(State state, int depth, Node parent)
    {
        this.state = state;
        this.depth = depth;
        this.parent = parent;
    }

    @Override
    public Node[] expand()
    {
        //System.out.println("Expanding: " + state.toString());
        expansions++;
        State[] states = state.getPossibleNextStates();
        Node[] nodes = new Node[states.length];
        for(int i = 0; i < states.length; i++) nodes[i] = new GameNode(states[i], depth+1, this);
        return nodes;
    }

    @Override
    public boolean isSolution()
    {
        return state.isEndState();
    }

    @Override
    public int getHeuristic()
    {
        return state.distanceFromGoal();
    }

    @Override
    public State getState()
    {
        return state;
    }

    public int getDepth()
    {
        return depth;
    }

    @Override
    public Node parent()
    {
        return parent;
    }

    @Override
    public long getExpansions()
    {
        return expansions;
    }
}
