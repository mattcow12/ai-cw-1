package me.mawood.game;

import java.awt.*;

/**
 * ai-cw-1 - PACKAGE_NAME
 * Created by MAWood on 30/11/2016.
 */
public class Tile
{
    private final Point goalPos;
    private Point position;
    private final String name;
    private final boolean movable;

    Tile(String name, Point startPos, Point goalPos, boolean movable)
    {
        this.name = name;
        this.position = startPos;
        this.goalPos = goalPos;
        this.movable = movable;
    }

    public Tile(String name, Point startPos, Point goalPos)
    {
        this(name,startPos,goalPos,true);
    }
    public Tile(String name, Point startPos)
    {
        this(name,startPos,startPos,false);
    }

    int getManhattanDistanceFromGoal()
    {
        return Math.abs(goalPos.x-position.x) + Math.abs(goalPos.y-position.y);
    }

    double getPythagorianDistanceFromGoal()
    {
        return position.distance(goalPos);
    }

    public Point getPosition()
    {
        return position;
    }

    void setPosition(Point position)
    {
        if(movable) this.position = position;
    }

    void setPosition(int x, int y)
    {
        setPosition(new Point(x,y));
    }

    public String getName()
    {
        return name;
    }

    Point getGoalPos()
    {
        return goalPos;
    }

    public boolean isMovable()
    {
        return movable;
    }
}
