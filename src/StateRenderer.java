//  GifSequenceWriter.java
//
//  Created by Elliot Kroo on 2009-04-25.
//
// This work is licensed under the Creative Commons Attribution 3.0 Unported
// License. To view a copy of this license, visit
// http://creativecommons.org/licenses/by/3.0/ or send a letter to Creative
// Commons, 171 Second Street, Suite 300, San Francisco, California, 94105, USA.


import me.mawood.game.*;

import javax.imageio.*;
import javax.imageio.metadata.*;
import javax.imageio.stream.*;
import java.awt.*;
import java.awt.image.*;
import java.io.*;
import java.util.*;

class StateRenderer
{
    private ImageWriter gifWriter;
    private ImageWriteParam imageWriteParam;
    private IIOMetadata imageMetaData;

    /**
     * Creates a new StateRenderer
     *
     * @param outputStream the ImageOutputStream to be written to
     * @param imageType one of the imageTypes specified in BufferedImage
     * @param timeBetweenFramesMS the time between frames in miliseconds
     * @param loopContinuously wether the gif should loop repeatedly
     * @throws IIOException if no gif ImageWriters are found
     *
     * @author Elliot Kroo (elliot[at]kroo[dot]net)
     */
    private StateRenderer(
            ImageOutputStream outputStream,
            int imageType,
            int timeBetweenFramesMS,
            boolean loopContinuously) throws IOException {
        // my method to create a writer
        gifWriter = getWriter();
        imageWriteParam = gifWriter.getDefaultWriteParam();
        ImageTypeSpecifier imageTypeSpecifier =
                ImageTypeSpecifier.createFromBufferedImageType(imageType);

        imageMetaData =
                gifWriter.getDefaultImageMetadata(imageTypeSpecifier,
                        imageWriteParam);

        String metaFormatName = imageMetaData.getNativeMetadataFormatName();

        IIOMetadataNode root = (IIOMetadataNode)
                imageMetaData.getAsTree(metaFormatName);

        IIOMetadataNode graphicsControlExtensionNode = getNode(
                root,
                "GraphicControlExtension");

        graphicsControlExtensionNode.setAttribute("disposalMethod", "none");
        graphicsControlExtensionNode.setAttribute("userInputFlag", "FALSE");
        graphicsControlExtensionNode.setAttribute(
                "transparentColorFlag",
                "FALSE");
        graphicsControlExtensionNode.setAttribute(
                "delayTime",
                Integer.toString(timeBetweenFramesMS / 10));
        graphicsControlExtensionNode.setAttribute(
                "transparentColorIndex",
                "0");

        IIOMetadataNode commentsNode = getNode(root, "CommentExtensions");
        commentsNode.setAttribute("CommentExtension", "Created by MAH");

        IIOMetadataNode appExtensionsNode = getNode(
                root,
                "ApplicationExtensions");

        IIOMetadataNode child = new IIOMetadataNode("ApplicationExtension");

        child.setAttribute("applicationID", "NETSCAPE");
        child.setAttribute("authenticationCode", "2.0");

        int loop = loopContinuously ? 0 : 1;

        child.setUserObject(new byte[]{ 0x1, (byte) (loop & 0xFF), (byte)
                ((loop >> 8) & 0xFF)});
        appExtensionsNode.appendChild(child);

        imageMetaData.setFromTree(metaFormatName, root);

        gifWriter.setOutput(outputStream);

        gifWriter.prepareWriteSequence(null);
    }

    private void writeToSequence(RenderedImage img) throws IOException {
        gifWriter.writeToSequence(
                new IIOImage(
                        img,
                        null,
                        imageMetaData),
                imageWriteParam);
    }

    /**
     * Close this StateRenderer object. This does not close the underlying
     * stream, just finishes off the GIF.
     */
    private void close() throws IOException {
        gifWriter.endWriteSequence();
    }

    /**
     * Returns the first available GIF ImageWriter using
     * ImageIO.getImageWritersBySuffix("gif").
     *
     * @return a GIF ImageWriter object
     * @throws IIOException if no GIF image writers are returned
     */
    private static ImageWriter getWriter() throws IIOException {
        Iterator<ImageWriter> iter = ImageIO.getImageWritersBySuffix("gif");
        if(!iter.hasNext()) {
            throw new IIOException("No GIF Image Writers Exist");
        } else {
            return iter.next();
        }
    }

    /**
     * Returns an existing child node, or creates and returns a new child node (if
     * the requested node does not exist).
     *
     * @param rootNode the <tt>IIOMetadataNode</tt> to search for the child node.
     * @param nodeName the name of the child node.
     *
     * @return the child node, if found or a new node created with the given name.
     */
    private static IIOMetadataNode getNode(
            IIOMetadataNode rootNode,
            String nodeName) {
        int nNodes = rootNode.getLength();
        for (int i = 0; i < nNodes; i++) {
            if (rootNode.item(i).getNodeName().compareToIgnoreCase(nodeName)
                    == 0) {
                return((IIOMetadataNode) rootNode.item(i));
            }
        }
        IIOMetadataNode node = new IIOMetadataNode(nodeName);
        rootNode.appendChild(node);
        return(node);
    }

    /**
     public StateRenderer(
     BufferedOutputStream outputStream,
     int imageType,
     int timeBetweenFramesMS,
     boolean loopContinuously) {

     */

    private static void writeGIF(File outputFile, Collection<BufferedImage> images) throws Exception {
            // grab the output image type from the first image in the sequence

            // create a new BufferedOutputStream with the last argument
            ImageOutputStream output =
                    new FileImageOutputStream(outputFile);

            // create a gif sequence with the type of the first image, 1 second
            // between frames, which loops continuously
            StateRenderer writer =
                    //new StateRenderer(output, BufferedImage.TYPE_INT_ARGB, 500, false);
                    new StateRenderer(output, BufferedImage.TYPE_BYTE_GRAY, 500, false);

            // write out the first image to our sequence...
            for(BufferedImage image:images) writer.writeToSequence(image);

            writer.close();
            output.close();
    }

    private static final int TILE_SIZE = 20;

    private static BufferedImage stateToImage(GameState state)
    {
        BufferedImage image = new BufferedImage(
                (TILE_SIZE*state.getWidth()) + state.getWidth() - 1,
                (TILE_SIZE*state.getHeight()) + state.getHeight() - 1,
                //BufferedImage.TYPE_INT_ARGB); // create the image
                BufferedImage.TYPE_BYTE_GRAY); // create the image


        Graphics2D g = image.createGraphics();
        g.setColor(Color.white);

        g.fillRect(0,0,image.getWidth() - 1,image.getHeight() - 1); // draw the background

        g.setColor(Color.black);
        for(int i = 1; i<state.getWidth()+1;i++) // draw the grid lines
        {
            g.drawLine((i * TILE_SIZE) + (i - 1), 0, (i * TILE_SIZE) + (i - 1), image.getHeight() - 1);
        }
        for(int i = 1; i<state.getHeight()+1;i++) // draw the grid lines
        {
            g.drawLine(0, (i * TILE_SIZE) + (i - 1), image.getWidth() - 1, (i * TILE_SIZE) + (i - 1));
        }

        Font font = new Font("Arial", Font.BOLD, 20);
        g.setFont(font);
        FontMetrics fm = g.getFontMetrics();

        for (Tile tile:state.getTiles()) // draw the letters
        {
            if(tile.isMovable())
            {
                String text = tile.getName();
                int x = tile.getPosition().x;
                x = (((x-1)*(TILE_SIZE + 1)) + TILE_SIZE/2) - fm.stringWidth(text) / 2;
                int y = (state.getHeight() + 1) - tile.getPosition().y;
                y = (((y-1)*(TILE_SIZE + 1)) + TILE_SIZE/2) + fm.getHeight() / 3;
                g.drawString(text, x, y);
            } else
            {
                int x = tile.getPosition().x;
                int y = (state.getHeight() + 1) - tile.getPosition().y;
                g.fillRect((x-1)*TILE_SIZE + (x-1),(y-1)*TILE_SIZE + (y-1),TILE_SIZE,TILE_SIZE);
            }

        }

        font = new Font("Arial", Font.BOLD, 25);
        g.setFont(font);

        String text = "\u263A";
        int x = state.getPlayerPos().x;
        x = (((x-1)*(TILE_SIZE + 1)) + TILE_SIZE/2) - (2*fm.stringWidth(text) / 3);
        int y = (state.getHeight() + 1) - state.getPlayerPos().y;
        y = (((y-1)*(TILE_SIZE + 1)) + TILE_SIZE/2) + fm.getHeight() / 3;
        g.drawString(text, x, y); // draw the actor

        return image;
    }

    static void makeGifOfSolution(GameNode solution, File outputFile) throws Exception
    {

        Stack<State> stack = new Stack<>();
        Node node = solution;
        while(node != null)
        {
            stack.push(node.getState()); // reorder states so it goes from start to finish not finish to start
            node = node.parent();
        }

        ArrayList<BufferedImage> steps = new ArrayList<>();

        while(!stack.isEmpty()) steps.add(StateRenderer.stateToImage((GameState) stack.pop())); // collect images

        StateRenderer.writeGIF(outputFile, steps); // save the gif
    }

    static void makeCollageOfSolution(GameNode solution, File outputFile) throws IOException
    {
        Stack<State> stack = new Stack<>();
        Node node = solution;
        while(node != null)
        {
            stack.push(node.getState()); // reorder states so it goes from start to finish not finish to start
            node = node.parent();
        }

        ArrayList<BufferedImage> steps = new ArrayList<>();

        while(!stack.isEmpty()) steps.add(StateRenderer.stateToImage((GameState) stack.pop())); // collect images
        if(steps.size() > 0)
        {
            int height = steps.get(0).getHeight();
            int width = steps.get(0).getWidth();
            BufferedImage outputImage;

            final int border = 5;

            int gridSize = (int)Math.floor(Math.sqrt(steps.size())); // decide how many columns there should be by attempting to make it about square
            int extraRowsNeeded = (int)Math.ceil((steps.size() - gridSize*gridSize)/gridSize); // figure out how many extra rows are needed to fit all steps
            outputImage = new BufferedImage(width*gridSize + (gridSize-1)*border, height*(gridSize+extraRowsNeeded)+(gridSize-1+extraRowsNeeded)*border,BufferedImage.TYPE_BYTE_GRAY);

            Graphics g = outputImage.createGraphics();

            g.setColor(Color.BLACK);
            g.fillRect(0,0,outputImage.getWidth(),outputImage.getHeight()); // fill background with black, this will form the borders

            int x = 0;
            int y = 0;
            for(BufferedImage step:steps)
            {
                g.drawImage(step,x*(width+border),y*(height+border), null); // draw each step left to right, top to bot
                x++;
                if(x >= gridSize)
                {
                    x = 0;
                    y++;
                }
            }
            ImageIO.write(outputImage,"PNG",outputFile); // write to file
        }
    }
}