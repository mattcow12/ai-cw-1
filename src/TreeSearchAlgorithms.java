import me.mawood.game.Node;

import java.util.*;

/**
 * ai-cw-1 - PACKAGE_NAME
 * Created by MAWood on 23/10/2016.
 */
class TreeSearchAlgorithms
{
    private final static boolean DEBUG = false;

    static Node BFS(Node root)
    {
        Queue<Node> Q = new LinkedList<>(); // create a fifo queue
        Q.add(root); // add the root

        Node current;

        while(!Q.isEmpty()) // while the queue is not empty
        {
            current = Q.poll(); // pop the element
            if(DEBUG)System.out.println("Inspecting " + current.getState());
            if (current.isSolution())
                return current; // if it is a solution return it
            Q.addAll(Arrays.asList(current.expand())); // add the child nodes to the queue
        }

        return null; // if no solutions were found return null
    }

    static Node DFS(Node root)
    {
        if(root.isSolution()) return root;

        List<Node> children= Arrays.asList(root.expand());
        Collections.shuffle(children);

        Node result;
        for(Node child:children)
        {
            if(DEBUG)System.out.println("Exploring " + child.getState());
            result = DFS(child);
            if(result != null) return result;
        }
        return null;
    }

    static Node DLS(Node node, int limit)
    {
        if(node.isSolution()) return node;
        if(node.getDepth() > limit) return null;

        Node result;
        for(Node child:node.expand())
        {
            result = DLS(child,limit);
            if(result != null) return result;
        }
        return null;
    }

    static Node IDS(Node root)
    {
        int limit = 0;
        Node solution = null;
        while(solution == null)
        {
            if(DEBUG)System.out.println("Limit set at "+ limit);
            solution = DLS(root,limit);
            limit++;
        }
        if(DEBUG)System.out.println("Solution found at limit " + limit);
        if(DEBUG)System.out.println("Solution : "+ solution.getState().toString());
        return solution;
    }

    static Node aStar(Node root)
    {
        ArrayList<AStarPair> Q = new ArrayList<>();
        Q.add(new AStarPair(root));

        Node current;

        while(!Q.isEmpty())
        {
            Collections.sort(Q);
            if(DEBUG)System.out.println("Queue : "+ Arrays.toString(Q.toArray()).replace("\n",""));
            current = Q.get(0).node;
            Q.remove(Q.get(0));
            if(DEBUG)System.out.println("Exploring " + current.getState());
            if (current.isSolution()) return current;
            for(Node node:current.expand()) Q.add(new AStarPair(node));
        }
        return null;
    }

}

class AStarPair implements Comparable
{
    Node node;
    private int fn;

    AStarPair(Node node)
    {
        this.fn = node.getDepth() + node.getHeuristic();
        this.node = node;
    }

    @Override
    public int compareTo(Object o)
    {
        return fn-((AStarPair) o).fn;
    }

    public String toString()
    {
        return "Estimated Cost: " + Integer.toString(fn) + " from " + node.getState();
    }
}
